import pandas as pd
from sklearn.preprocessing import LabelEncoder
from matplotlib import pyplot as plt
import seaborn as sns
from sklearn.model_selection import train_test_split
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import confusion_matrix, accuracy_score, classification_report
import statsmodels.api as sm
from sklearn import tree
from sklearn import utils


class LoanPredictor:
    def __init__(self, X):
        self.X = X
        self.Xtrain = []
        self.Xtest = []
        self.ytrain = []
        self.ytest = []

    def cleanse_data(self):

        # drop irrelevant columns
        self.X.drop(['Loan_ID', 'Gender'], axis=1, inplace=True)  # , 'Married', 'Property_Area', 'Education'
        print(self.X.head())

        # check for null values
        print(self.X.isnull().sum().any())
        if self.X.isnull().sum().any():
            print(self.X.isnull().sum())

            # check one by one, and fill missing values

            # Credit History (1 - yes, 0 - no)
            print(self.X.Loan_Status.value_counts())
            print(self.X.Credit_History.value_counts())
            # if not entered assume there is no loan history
            self.X.Credit_History.fillna(0.0, inplace=True)

            # Loan Amount Term -> fill missing with mean term
            self.X.Loan_Amount_Term.fillna(self.X.Loan_Amount_Term.mean(), inplace=True)

            # Loan Amount Term -> fill missing with mean amount
            self.X.LoanAmount.fillna(df.LoanAmount.mean(), inplace=True)

            # Self Employed -> if missing assume that not self employed
            self.X.Self_Employed.fillna('No', inplace=True)

            # Married -> if null assume not married
            print(self.X.Married.value_counts())
            self.X.Married.fillna('No', inplace=True)

            # Dependents -> if null assume 0 (1 , 2 , 3+) -> difficult to include in a linear model
            # print(self.X.Dependents.value_counts())
            # self.X.Dependents.fillna(0, inplace=True)
            dummy = pd.get_dummies(self.X['Dependents'])
            print(dummy.head())
            self.X = pd.concat([self.X, dummy], axis=1)
            self.X.drop('Dependents', axis=1, inplace=True)
            self.X.drop('0', axis=1, inplace=True)
            print(self.X.head())

            # Property_Area
            dummy = pd.get_dummies(self.X['Property_Area'])
            print(dummy.head())
            self.X = pd.concat([self.X, dummy], axis=1)
            self.X.drop('Property_Area', axis=1, inplace=True)
            self.X.drop('Rural', axis=1, inplace=True)
            print(self.X.head())


            # All cleaned up
            print(self.X.isnull().sum().any())

            # Look at types of variables
            self.X['Credit_History'] = self.X['Credit_History'].astype('int64')
            print(self.X.dtypes)

            # Fit object types (Yes/No; Graduated/Not Graduated) to 0/1
            le = LabelEncoder()
            cols = self.X.columns.tolist()
            for column in cols:
                print(column)
                if self.X[column].dtype == 'object':
                    self.X[column] = le.fit_transform(self.X[column])

            print(self.X.dtypes)
            print(self.X.head())
            print(self.X['Loan_Status'])

    def eda(self):
        fig, ax = plt.subplots(figsize=(20, 15))
        sns.heatmap(data=self.X.corr().round(2), annot=True, linewidths=0.7, cmap='Blues')
        plt.savefig('corr.png', bbox_inches='tight')
        #
        # fig, ax = plt.subplots(figsize=(20, 15))
        # sns.boxplot(x=self.X['CoapplicantIncome'])
        # plt.show()
        #
        # fig, ax = plt.subplots(figsize=(20, 15))
        # sns.boxplot(x=self.X['LoanAmount'])
        # plt.show()
        #
        # fig, ax = plt.subplots(figsize=(20, 15))
        # sns.boxplot(x=self.X['Loan_Amount_Term'])
        # plt.show()

        # remove outliers
        # print(self.X.shape)
        # Q1 = self.X.quantile(0.25)
        # Q3 = self.X.quantile(0.75)
        # IQR = Q3 - Q1
        # print(IQR)
        # self.X = self.X[~((X < (Q1-1.5 * IQR)) | (df > (Q3 + 1.5 * IQR))).any(axis=1)]
        # print(self.X.shape)

    def split_test_train(self):
        y = self.X["Loan_Status"]
        self.X = self.X.drop("Loan_Status", axis=1)

        self.Xtrain, self.Xtest, self.ytrain, self.ytest = train_test_split(self.X, y, test_size=0.3, random_state=30)

    def upsample(self):

        # Separate majority and minority classes
        df_minority = self.X[self.X.Loan_Status == 0]
        df_majority = self.X[self.X.Loan_Status == 1]

        # Upsample minority class
        df_minority_upsampled = utils.resample(df_minority,
                                         replace=True,     # sample with replacement
                                         n_samples=df_majority.shape[0],    # to match majority class
                                         random_state=30) # reproducible results

        df_upsampled = pd.concat([df_majority, df_minority_upsampled])

        print(df_upsampled.Loan_Status.value_counts())
        self.X = df_upsampled

    def predictLogReg(self):
        # Logistic regression
        model = LogisticRegression()
        model.fit(self.Xtrain, self.ytrain)

        # stats model logistic regression
        log_reg = sm.Logit(self.ytrain, self.Xtrain).fit()
        print(log_reg.summary())

        y_pred = model.predict(self.Xtest)
        print("Accuracy: %s%%" % (100 * accuracy_score(y_pred, self.ytest)))
        print(confusion_matrix(self.ytest, y_pred))
        labels = ['True Neg','False Pos','False Neg','True Pos']
        categories = ['Not Approved', 'Approved']

        ax= plt.subplot()
        sns.heatmap(confusion_matrix(self.ytest, y_pred), annot=True, fmt='g', ax=ax)  #annot=True to annotate cells, ftm='g' to disable scientific notation

        # labels, title and ticks
        ax.set_xlabel('Predicted labels')
        ax.set_ylabel('True labels')
        ax.set_title('Confusion Matrix')
        ax.xaxis.set_ticklabels(['Not Approved', 'Approved'])
        ax.yaxis.set_ticklabels(['Not Approved', 'Approved'])
        plt.savefig('cf_LogReg.png', bbox_inches='tight')
        print("Classification Report for Logistic Regression")
        print(classification_report(self.ytest, y_pred))

    def predictDecisionTree(self):
        # Decision Tree
        model = DecisionTreeClassifier(max_depth=4   )
        model.fit(self.Xtrain, self.ytrain)

        fig = plt.figure(figsize=(25, 20))
        _ = tree.plot_tree(model,
                           feature_names=self.Xtest.columns,
                           class_names=["Not Approved", "Approved"],
                           filled=True)
        plt.savefig('tree.png', bbox_inches='tight')

        # path = model.cost_complexity_pruning_path(self.Xtrain, self.ytrain)
        # ccp_alphas, impurities = path.ccp_alphas, path.impurities
        #
        # for ccp_alpha in ccp_alphas:
        #     clf = DecisionTreeClassifier(random_state=0, ccp_alpha=ccp_alpha)
        #     clf.fit(self.Xtrain, self.ytrain)
        #     fig = plt.figure(figsize=(25, 20))
        #     _ = tree.plot_tree(clf,
        #                        feature_names=self.Xtest.columns,
        #                        class_names=["Y", "N"],
        #                        filled=True)
        #     plt.show()

        y_pred = model.predict(self.Xtest)
        print("Accuracy: %s%%" % (100 * accuracy_score(y_pred, self.ytest)))
        print(confusion_matrix(self.ytest, y_pred))
        print("Classification Report for Decision Tree Classifier")
        print(classification_report(self.ytest, y_pred))
        ax= plt.subplot()
        sns.heatmap(confusion_matrix(self.ytest, y_pred), annot=True, fmt='g', ax=ax)  #annot=True to annotate cells, ftm='g' to disable scientific notation

        # labels, title and ticks
        ax.set_xlabel('Predicted labels')
        ax.set_ylabel('True labels')
        ax.set_title('Confusion Matrix')
        ax.xaxis.set_ticklabels(['Not Approved', 'Approved'])
        ax.yaxis.set_ticklabels(['Not Approved', 'Approved'])
        plt.savefig('cf_DecisionTree.png', bbox_inches='tight')

    def predictRandomForest(self):
        # Random Forest
        model = RandomForestClassifier()
        model.fit(self.Xtrain, self.ytrain)

        y_pred = model.predict(self.Xtest)
        print("Accuracy: %s%%" % (100 * accuracy_score(y_pred, self.ytest)))
        print(confusion_matrix(self.ytest, y_pred))
        print("Classification Report for Random Forest Classifier")
        print(classification_report(self.ytest, y_pred))
        ax= plt.subplot()
        sns.heatmap(confusion_matrix(self.ytest, y_pred), annot=True, fmt='g', ax=ax)  #annot=True to annotate cells, ftm='g' to disable scientific notation

        # labels, title and ticks
        ax.set_xlabel('Predicted labels')
        ax.set_ylabel('True labels')
        ax.set_title('Confusion Matrix')
        ax.xaxis.set_ticklabels(['Not Approved', 'Approved'])
        ax.yaxis.set_ticklabels(['Not Approved', 'Approved'])
        plt.savefig('cf_RandomForest.png', bbox_inches='tight')


if __name__ == "__main__":
    path = "loan_data_set.csv"
    df = pd.read_csv(path)
    loan_predictor = LoanPredictor(df)
    loan_predictor.cleanse_data()
    loan_predictor.eda()
    loan_predictor.upsample()
    loan_predictor.split_test_train()
    loan_predictor.predictLogReg()
    loan_predictor.predictDecisionTree()
    loan_predictor.predictRandomForest()



